#include <AccelStepper.h>
#define HALFSTEP 8

// motor pins
#define motorPin1  3     // IN1 on the ULN2003 driver 1
#define motorPin2  4     // IN2 on the ULN2003 driver 1
#define motorPin3  5     // IN3 on the ULN2003 driver 1
#define motorPin4  6     // IN4 on the ULN2003 driver 1

#define motorPin5  8     // IN1 on the ULN2003 driver 2
#define motorPin6  9     // IN2 on the ULN2003 driver 2
#define motorPin7  10    // IN3 on the ULN2003 driver 2
#define motorPin8  11    // IN4 on the ULN2003 driver 2

// Initialize with pin sequence IN1-IN3-IN2-IN4 for using the AccelStepper with 28BYJ-48
AccelStepper stepper1(HALFSTEP, motorPin1, motorPin3, motorPin2, motorPin4);
AccelStepper stepper2(HALFSTEP, motorPin5, motorPin7, motorPin6, motorPin8);

// variables
float maxspeed = 2000;
int lineSteps = -280; //number of steps to drive 1cm straight
int stepperSpeed = 1000; //speed of the stepper (steps per second)
int steps1 = 0; // keep track of the step count for motor 1
int steps2 = 0; // keep track of the step count for motor 2
float bigwheel_diameter = 56.89;
float motorshaft_diameter = 5.0;
float ratio = bigwheel_diameter / motorshaft_diameter;
float wheel_base = 78.21;
int steps_rev = 512 * 8;

boolean turn1 = false; //keep track if we are turning or going straight next
boolean turn2 = false; //keep track if we are turning or going straight next


int distanceToStep(float distance) {
  int steps = distance * steps_rev / (bigwheel_diameter * 3.1412); //24.61
  return steps;
}

void move_forward(int steps) {
  steps *= abs(lineSteps);
  stepper1.move(steps);
  stepper1.setSpeed(stepperSpeed);

  stepper2.move(-steps);
  stepper2.setSpeed(stepperSpeed);
}



void turn(float degrees) {
  //float rotation = degrees / 360.0;
  float rotation = getNearestAngle(degrees) / 360.0;
  float distance = wheel_base * 3.1412 * rotation;
  distance = distanceToStep(distance);
  stepper1.move(-distance);
  stepper1.setSpeed(stepperSpeed);

  stepper2.move(-distance);
  stepper2.setSpeed(stepperSpeed);
//  return distance;
}


void setup() {
  delay(3000); //sime time to put the robot down after swithing it on

  stepper1.setMaxSpeed(maxspeed);
  stepper1.move(1);  // I found this necessary
  stepper1.setSpeed(stepperSpeed);

  stepper2.setMaxSpeed(maxspeed);
  stepper2.move(-1);  // I found this necessary
  stepper2.setSpeed(stepperSpeed);
  Serial.begin(9600);
}
float getNearestAngle(float angle_) {
  // code contributed by Instructable user PMPU_student
  /*
    Lets rotate by 58 degrees.
    turnRight(58); // rotate only by 57.631138392857146 degrees(that is the
    value that is the closest to the desired one _and_ is lesser than it).
    That means you are doing a certain amount of motor steps. But if you do
    one more step during rotation you will rotate by 58.0078125 degrees,
    which is much more precise(but it is greater than desired value, thats
    why in original code it is not picked). The thing is that original code
    always decides to rotate by value that is lesser than desired value while
    it is sometimes worse than choosing the value that is bigger.
    My code chooses from both variants, minimizing the error.
  */
  float angle = 0;
  int step = 0;
  float previousAngle = 0;
  float step_length = 3.1412 * bigwheel_diameter / 512;
  while (!(previousAngle <= angle_ && angle_ <= angle)) {
    step += 1;
    previousAngle = angle;
    angle = step * step_length * 360 / (wheel_base * 3.1412) + 0.01;
  }
  float dif1 = angle_ - angle;
  float dif2 = angle_ - previousAngle;
  if (abs(dif1) < abs(dif2)) {
    return angle;
  } else {
    return previousAngle;
  }
}
String sequence[] = {"f5", "r90", "f5", "r90", "f5", "r90", "f5", "r90"};
int sequenceNumber = 0;
void loop() {
  
  if (steps1 == 0 && sequenceNumber < 8) {
    String current_sequence = sequence[sequenceNumber];
    sequenceNumber++;
    if (current_sequence[0] == 'f')
    {
      move_forward(5);
    }
    else if (current_sequence[0] == 'r')
    {
      turn(-90);
    }
  }
  steps1 = stepper1.distanceToGo();
  //steps2 = stepper2.distanceToGo();

  stepper1.runSpeedToPosition();
  stepper2.runSpeedToPosition();
}
